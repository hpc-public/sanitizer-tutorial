#include <stdio.h>
#include <math.h>
#include <atomic>
int main(int argc, char** argv){
  std::atomic<int> a{0}; int l=100;
  if(argc>1)
    l = atoi(argv[1]);
  double sum=0;
#pragma omp parallel num_threads(8) shared(a) reduction(+:sum)
  while (a++ < l){
    sum += sin(a.load());
  }
  printf("a=%i, sum=%lf\n", a.load(), sum);
}

