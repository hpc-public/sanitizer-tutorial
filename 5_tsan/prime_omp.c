/*
 * Copyright (c) 2006, 2010, Oracle and/or its affiliates. All Rights Reserved.
 * @(#)prime_omp.c 1.3 (Oracle) 10/03/26
 * Downloaded at https://docs.oracle.com/cd/E37069_01/html/E37080/geazt.html
 */

#include <stdio.h>
#include <math.h>

#define N 1000000

int primes[N];
int pflag[N];

int is_prime(int v)
{
 int i;
 int bound = floor(sqrt(v)) + 1;

 for (i = 2; i < bound; i++) {
        /* no need to check against known composites */
        if (!pflag[i])
            continue;
        if (v % i == 0) {
            pflag[v] = 0;
            return 0;
        }
    }
    return (v > 1);
}

int main(int argn, char **argv)
{
    int i;
    int total = 0;

    for (i = 0; i < N; i++) {
        pflag[i] = 1;
    }

    #pragma omp parallel for schedule(dynamic)
    for (i = 2; i < N; i++) {
        if ( is_prime(i) ) {
            primes[total++] = i;
        }
    }

    printf("Number of prime numbers between 2 and %d: %d\n",
           N, total);

    return 0;
}
