#include <stdio.h>
#include <math.h>

int main(int argc, char** argv){
  int a = 0, l = 100;
  if(argc>1)
    l = atoi(argv[1]);
  double sum=0;
#pragma omp parallel num_threads(8) shared(a) reduction(+:sum)
  while (a < l){
    sum+=sin(a++);
  }
  printf("a=%i, sum=%lf\n", a, sum);
}

